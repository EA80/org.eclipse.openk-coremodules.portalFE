/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { By } from '@angular/platform-browser';
import { Injectable, DebugElement } from '@angular/core';
import { click, newEvent } from '../../testing';
import { NgModule } from '@angular/core';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { MockComponent } from '../../testing/mock.component';
import { MdDialogModule, MaterialModule, MdDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { LogoutComponent } from './logout.component';
import { SessionContext } from '../../common/session-context';
import { AuthenticationService } from '../../services/authentication.service';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

@NgModule({
  declarations: [],
  entryComponents: [
    LogoutComponent
  ]
})
class TestModule { }

describe('LogoutComponent', () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;
  let routerStub: FakeRouter;
  let router: Router;
  
  routerStub = {
    navigate: jasmine.createSpy('navigate').and.callThrough()
  };
  
  class MockAuthService extends AbstractMockObservableService {
    loadCalled = false;

    public logout(value: boolean) {
      this.loadCalled = true;
      return this;
    }
  }

  class MockDialogRef extends AbstractMockObservableService {
    close() { }
    logout() { }
  }

  let mockAuthService;

  beforeEach(async(() => {
    mockAuthService = new MockAuthService();
    router = new FakeRouter() as any as Router;
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        MaterialModule.forRoot()
      ],
      declarations: [ 
        LogoutComponent,        
        MockComponent({ selector: 'input', inputs: ['options'] })
      ],
      providers: [
        { provide: AuthenticationService, useValue: mockAuthService },
        { provide: Router, useValue: routerStub },
        { provide: MdDialogRef, useClass: MockDialogRef },
        { provide: SessionContext, useClass: SessionContext }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
  });


  it('should close dialog on Nein-Button', async(() => {
    spyOn(component.dialogRef, 'close').and.callThrough();
    
    fixture.detectChanges();

    const de = fixture.debugElement.query(By.css('button:nth-child(2)'));
    click(de);

    fixture.whenStable().then(() => {
      expect(component.dialogRef.close).toHaveBeenCalled();
    });

  }));

  it('should call logout on Ja-Button', async(() => {
    spyOn(component, 'logout').and.callThrough();
    
    fixture.detectChanges();

    const de = fixture.debugElement.query(By.css('button:nth-child(1)'));
    click(de);    
    fixture.whenStable().then(() => {
      expect(component.logout).toHaveBeenCalled();
    });

  }));

});
