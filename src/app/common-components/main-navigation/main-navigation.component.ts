/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, OnInit } from '@angular/core';
import { SessionContext } from '../../common/session-context';
import { MdDialog, MdDialogConfig } from '@angular/material';
import { LogoutComponent } from '../../dialogs/logout/logout.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.css']
})
export class MainNavigationComponent implements OnInit {

  private dialogConfig = new MdDialogConfig();
  
  constructor(
    public sessionContext: SessionContext,
    public dialog: MdDialog,
    private router: Router
  ) { }

  ngOnInit() {
     this.dialogConfig.disableClose = true;
  }

  openDialogLogout() {
    this.dialogConfig.data = this;
    const dialogRef = this.dialog.open(LogoutComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  logout() {
    this.sessionContext.clearStorage();
    this.router.navigate(['/']);
  }

}
