/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpModule, Http, XHRBackend, Response, ResponseOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

import { VersionInfoService } from './version-info.service';
import { VersionInfo } from '../model/version-info';
import { LoginCredentials } from '../model/login-credentials';
import { User } from '../model/user';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';

describe('Http-VersionInfoService (mockBackend)', () => {
  let sessionContext: SessionContext;

  beforeEach( async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpModule ],
      providers: [
        VersionInfoService,
        { provide: XHRBackend, useClass: MockBackend },
        SessionContext
      ]
    })
    .compileComponents();
    sessionContext = new SessionContext();
  }));

  it('can instantiate service when inject service',
    inject([VersionInfoService], (service: VersionInfoService) => {
      expect(service instanceof VersionInfoService).toBe(true);
  }));

  it('can instantiate service with "new"', inject([Http], (http: Http) => {
    expect(http).not.toBeNull('http should be provided');
    const service = new VersionInfoService(http, sessionContext );
    expect(service instanceof VersionInfoService).toBe(true, 'new service should be ok');
  }));


  it('can provide the mockBackend as XHRBackend',
    inject([XHRBackend], (backend: MockBackend) => {
      expect(backend).not.toBeNull('backend should be provided');
  }));


  describe('when loadBackendServerInfo()', () => {
    let backend: MockBackend;
    let service: VersionInfoService;
    let fakeVersion: VersionInfo;
    let response: Response;


    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new VersionInfoService(http, sessionContext);
      fakeVersion = { backendVersion: '2.2', dbVersion: '3.3' };
      const options = new ResponseOptions({status: 200, body: fakeVersion});
      response = new Response(options);
    }));
    it('should have expected fake versionInfo (then)', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.loadBackendServerInfo().toPromise()
          .then( versionInfo => {
            const be = versionInfo.backendVersion;
            expect(versionInfo.backendVersion).toBe('2.2');
          });
    })));

    it('should treat 404 as an Observable error', async(inject([], () => {
        const resp = new Response(new ResponseOptions({status: 404}));
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

        service.loadBackendServerInfo()
          .do(versionInfo => {
            fail('should not respond with versionInfo');
          })
          .catch(err => {
            const str = err;
            expect(err).toMatch('Bad response status', 'should catch bad response status code');
            return Observable.of(null); // failure is the expected test result
          })
          .toPromise();
      })));
  });

});
