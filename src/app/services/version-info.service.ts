/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { SessionContext } from '../common/session-context';
import { BaseHttpService } from './base-http.service';
import { VersionInfo } from '../model/version-info';

@Injectable()
export class VersionInfoService extends BaseHttpService {

    constructor(
        private _http: Http,
        private _sessionContext: SessionContext ) {
    super();
    }

    public loadBackendServerInfo(): Observable<VersionInfo> {
        const headers = new Headers();
        this.createCommonHeaders(headers, this._sessionContext);
        return this._http.get(super.getBaseUrl() + '/versionInfo', { headers: headers })
            .map ( resp => super.extractData( resp, this._sessionContext  ) )
            .catch(err => super.handleErrorPromise(err, this._sessionContext)  );
    }
}
