/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
export enum StatusEn {
    open = 1,
    inWork = 2,
    done = 3,
    closed = 4
}

export enum BannerMessageStatusEn {
    warning = 1,
    success = 2,
    error = 3,
    info = 4
}
