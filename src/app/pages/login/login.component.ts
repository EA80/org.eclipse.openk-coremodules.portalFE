/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, OnInit, ViewChildren, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params, ParamMap } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { BaseDataService } from '../../services/base-data.service';
import { SessionContext } from '../../common/session-context';
import { Globals } from '../../common/globals';
import { LoginCredentials } from '../../model/login-credentials';
import { User } from '../../model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit, AfterViewInit {
  @ViewChildren('username') usernameInput;

  private urlParamsMap: ParamMap;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private baseDataService: BaseDataService,
    public sessionContext: SessionContext,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.urlParamsMap = this.activatedRoute.snapshot.queryParamMap;
    localStorage.removeItem('authenticatedUserId');
  }

  ngAfterViewInit() {
    this.usernameInput.first.nativeElement.focus();
  }


  private pastLogin() {
    this.processDirectLinkToGridMeasureDetail();
    this.initBaseData();
    this.goToOverview();
  }

  private processDirectLinkToGridMeasureDetail() {
    if (!this.urlParamsMap) {
      return;
    }
    const fwdUrl = this.urlParamsMap.get('fwdUrl');
    const fwdId = this.urlParamsMap.get('fwdId');
    if (!fwdUrl) {
      return;
    }

    const linkToOpen = fwdUrl + '?accessToken=' + this.sessionContext.getAccessToken() + '&fwdId=' + fwdId;
    this.urlParamsMap = undefined;

    this.authService.checkAuth().subscribe(res => {
      window.open(linkToOpen, '_blank');
    },
      error => {
        console.log(error);
      });
  }

  private initBaseData() {
  }

  private setError(showErr: boolean) {
    if (showErr) {
      document.getElementById('error-message').classList.remove('hidden');
    } else {
      document.getElementById('error-message').classList.add('hidden');
    }
  }

  public login(name: string, pw: string) {
    this.setError(false);
    const creds = new LoginCredentials();
    creds.userName = name;
    creds.password = pw;
    this.authService.login(creds)
      .subscribe(jwtToken => this.onLoggedInSuccessfully(jwtToken),
        error => {
          console.log(error);
          this.setError(true);
        });
  }

  private onLoggedInSuccessfully(jwtToken: any) {
    this.sessionContext.setAccessToken(jwtToken.access_token);
    this.pastLogin();
  }

  public goToOverview() {
    this.router.navigate(['/overview']);
  }
}
