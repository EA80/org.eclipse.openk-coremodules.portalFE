/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* Model for the contents of the accessToken, decoded and parsed. It is not completed,
only the properties actually used are defined. To be completed if needed. */
export class JwtPayload {
    name: string;
    roles: string[];
}