/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, Optional } from '@angular/core';
import { Router } from '@angular/router';
import { MdDialogRef } from '@angular/material';
import { SessionContext } from '../../common/session-context';
import { Globals } from '../../common/globals';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
})
export class LogoutComponent {

  constructor(
    private router: Router,
    private sessionContext: SessionContext,
    private authService: AuthenticationService,
    @Optional() public dialogRef: MdDialogRef<LogoutComponent>) { }

  logout() {    
    this.authService.logout().subscribe(res => {
      this.dialogRef.config.data.logout();    
      this.dialogRef.close();
      this.router.navigate(['/login']);
    },
    error => {
      console.log(error);      
    });    
  }
}
