/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { By } from '@angular/platform-browser';
import { click } from '../../testing/index';
import { NgModule } from '@angular/core';
import { DebugElement } from '@angular/core';
import { MockComponent } from '../../testing/mock.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '../../testing/router-stubs';
import { MdDialogModule, MaterialModule, MdDialog } from '@angular/material';
import { SessionContext } from '../../common/session-context';
import { MainNavigationComponent } from './main-navigation.component';
import { JWT_ACCESS_TOKEN_HUGO } from '../../testing/jwt-token';
import { JWT_TOKEN_HUGO } from '../../testing/jwt-token';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

@NgModule({
  declarations: [],
  entryComponents: [
  ]
})
class TestModule { }

describe('MainNavigationComponent', () => {
  let component: MainNavigationComponent;
  let fixture: ComponentFixture<MainNavigationComponent>;
  let routerStub: FakeRouter;
  let router: Router;  

  routerStub = {
    navigate: jasmine.createSpy('navigate').and.callThrough()
  };

  beforeEach(async(() => {    
    router = new FakeRouter() as any as Router;
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        RouterTestingModule.withRoutes([]),
        MaterialModule.forRoot()
      ],
      declarations: [ 
        MainNavigationComponent,
        MockComponent({ selector: 'input', inputs: ['options'] })
      ],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: MdDialog, useClass: MdDialog },
        { provide: SessionContext, useClass: SessionContext }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainNavigationComponent);
    component = fixture.componentInstance;    
  });

  it('should navigate to mainview on logout', () => {    
    component.sessionContext.setAccessToken(JWT_TOKEN_HUGO); 
    component.logout();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/']);
  });

});
