/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SessionContext } from './session-context';
import { User } from '../model/user';

describe('SessionContext', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SessionContext]
    });

  });

  it('can instatiate service whe inject service', inject([SessionContext], (service: SessionContext) => {
    expect(service instanceof SessionContext);
  }));
});
