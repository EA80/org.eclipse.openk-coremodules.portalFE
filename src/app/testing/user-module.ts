/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { UserModule } from '../model/user-module';

export const EMPTY_USER_MODULES: UserModule[] = [

];

export const USER_MODULES: UserModule[] = [
    {
        name: 'Betriebstagebuch',
        cols: 1,
        rows: 1,
        color: '#ffffff',
        link: '/elogbookFE',
        pictureLink: 'https://www.openkonsequenz.de/medien/cache/main_image/m_logbuch_2443636.jpg',
        requiredRole: 'elogbook_access'
    },
    {
        name: 'Bereitschaftsplanung',
        cols: 1,
        rows: 1,
        color: '#ffffff',
        link: 'https://www.openkonsequenz.de/anwender/11-geplante-projekte/94-bereitschaftsplan',
        pictureLink: 'https://www.openkonsequenz.de/medien/cache/main_image/l_bereitschaftsplan_57882047.jpg',
        requiredRole: 'planning_access'
    },
    {
        name: 'Einspeisemanagement',
        cols: 1,
        rows: 1,
        color: '#ffffff',
        link: 'https://www.openkonsequenz.de/anwender/11-geplante-projekte/20-eisman',
        pictureLink: 'https://www.openkonsequenz.de/medien/cache/main_image/l_ok_module6.jpg',
        requiredRole: 'feedin_management_access'
    },
    {
        name: 'Geplante Maßnahmen',
        cols: 1,
        rows: 1,
        color: '#ffffff',
        link: 'https://www.openkonsequenz.de/anwender/11-geplante-projekte/64-geplante-netzmassnahme',
        // tslint:disable-next-line:max-line-length
        pictureLink: 'https://www.openkonsequenz.de//components/com_flexicontent/librairies/phpthumb/phpThumb.php?src=/medien/cache/main_image/l_ok_module5.jpg',
        requiredRole: 'planned_policies_access'
    }
];

export const ONE_USER_MODULE: UserModule[] = [
    {
        name: 'Betriebstagebuch',
        cols: 1,
        rows: 1,
        color: '#ffffff',
        link: '/elogbookFE',
        pictureLink: 'https://www.openkonsequenz.de/medien/cache/main_image/m_logbuch_2443636.jpg',
        requiredRole: 'elogbook_access'
    }
];

export const PGM_MODULE: UserModule =
    {
        name: 'Geplante Maßnahmen',
        cols: 1,
        rows: 1,
        color: '#ffffff',
        link: 'http://localhost:4220',
        // tslint:disable-next-line:max-line-length
        pictureLink: 'https://www.openkonsequenz.de//components/com_flexicontent/librairies/phpthumb/phpThumb.php?src=/medien/cache/main_image/l_ok_module5.jpg',
        requiredRole: 'planned_policies_access'
    }
    ;