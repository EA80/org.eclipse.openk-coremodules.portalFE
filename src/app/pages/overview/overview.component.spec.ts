/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */
import { async, fakeAsync, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { By } from '@angular/platform-browser';
import { click } from '../../testing/index';
import { DebugElement, EventEmitter } from '@angular/core';
import { MainNavigationComponent } from '../../common-components/main-navigation/main-navigation.component';
import { MockComponent } from '../../testing/mock.component';
import { OverviewComponent } from './overview.component';
import { Router } from '@angular/router';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { MdDialogModule, MaterialModule, MdDialog, MdDialogConfig, Overlay, OverlayContainer, OVERLAY_PROVIDERS } from '@angular/material';
import { SessionContext } from '../../common/session-context';
import { AppModule } from '../../app.module';
import { NgModule } from '@angular/core';
import { ModuleGridComponent } from '../module-grid/module-grid.component';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}
@NgModule({
  declarations: [],
  entryComponents: []
})
class TestModule { }

describe('OverviewComponent', () => {
  const sessionContext: SessionContext = new SessionContext();
  let component: OverviewComponent;
  let fixture: ComponentFixture<OverviewComponent>;
  let router: Router;
 
  beforeEach(async(() => {
    
    router = new FakeRouter() as any as Router;

    TestBed.overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: []
      }
    });

    TestBed.configureTestingModule({
      imports: [
        MdDialogModule,
        MaterialModule.forRoot()],
      declarations: [
        OverviewComponent,
        MainNavigationComponent,     
        MockComponent({ selector: 'input', inputs: ['options'] }),
        MockComponent({
          selector: 'app-module-grid'
        })
      ],
      providers: [
        { provide: Router, useValue: router },
        { provide: Overlay, useClass: Overlay },
        { provide: OVERLAY_PROVIDERS, useClass: OVERLAY_PROVIDERS },
        { provide: SessionContext, useValue: sessionContext }
      ],
    }).compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewComponent);
    component = fixture.componentInstance;
  });

  it('should be created', () => {
    expect(component).toBeTruthy();    
  });

  
});
