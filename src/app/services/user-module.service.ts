/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { UserModule } from '../model/user-module';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';
import { BaseHttpService } from './base-http.service';


@Injectable()
export class UserModuleService extends BaseHttpService {

    constructor(
        private _http: Http,
        private _sessionContext: SessionContext) {
        super();
    }

    public getUserModulesForUser(): Observable<UserModule[]> {
        const headers = new Headers();
        const url = super.getBaseUrl() + '/userModulesForUser/';
        this.createCommonHeaders(headers, this._sessionContext);

        return this._http.get(url, { headers: headers })
            .map(res => super.extractData(res, this._sessionContext))
            .catch(err => super.handleErrorPromise(err, this._sessionContext));
    }
}