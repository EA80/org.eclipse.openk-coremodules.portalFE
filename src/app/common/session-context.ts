/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable, EventEmitter } from '@angular/core';
import { JwtHelper } from 'angular2-jwt';
import { BannerMessageStatusEn } from '../common/enums';
import { Globals } from '../common/globals';
import { JwtPayload } from '../model/jwt-payload';


@Injectable()
export class SessionContext {
    public centralHttpResultCode$: EventEmitter<number> = new EventEmitter<number>();
    public settings;
    private my: string;

    public getCurrSessionId(): string { return localStorage.getItem(Globals.LOCALSTORAGE_SESSION_ID); }
    public setCurrSessionId(sid: string): void { localStorage.setItem(Globals.LOCALSTORAGE_SESSION_ID, sid); }

    public clearStorage() {
        localStorage.clear();
    }

    public getAccessToken(): string {
        return localStorage.getItem(Globals.ACCESS_TOKEN);
    }

    public setAccessToken(accessToken: string): void {
        localStorage.setItem(Globals.ACCESS_TOKEN, accessToken);
    }

    public getAccessTokenDecoded(): JwtPayload {
        const accessToken = this.getAccessToken();
        if (!accessToken) {
            return;
        }
        const jwtHelper: JwtHelper = new JwtHelper();
        const jwtPayload: JwtPayload = new JwtPayload();
        const decoded: any = jwtHelper.decodeToken(accessToken);
        jwtPayload.roles = [];
        jwtPayload.name = decoded.name;

        //add realm roles to jwtPayload.roles
        if (decoded.realm_access && decoded.realm_access.roles) {
          jwtPayload.roles = decoded.realm_access.roles;
        }

        //add client roles to jwtPayload.roles
        const clientRoles = [];
        const resourceAccessList = decoded.resource_access;
        for (const el in resourceAccessList) {
          clientRoles.push(...resourceAccessList[el].roles);
        }
        jwtPayload.roles.push(...clientRoles);

        return jwtPayload;
    }
}
