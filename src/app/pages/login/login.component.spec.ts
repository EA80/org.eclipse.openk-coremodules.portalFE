/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router, ActivatedRoute } from '@angular/router';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { MockComponent } from '../../testing/mock.component';
import { AuthenticationService } from '../../services/authentication.service';
import { BaseDataService } from '../../services/base-data.service';
import { LoginComponent } from './login.component';
import { VersionInfoComponent } from '../../common-components/version-info/version-info.component';
import { Globals } from '../../common/globals';
import { User } from '../../model/user';
import { LoginCredentials } from '../../model/login-credentials';
import { SessionContext } from '../../common/session-context';
import { JwtHelper } from 'angular2-jwt';
import { JWT_TOKEN_HUGO } from '../../testing/jwt-token';
import { ActivatedRouteStub } from '../../testing';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

describe('LoginComponent', () => {
  let router: Router;
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let spy: jasmine.Spy;
  let activatedStub: ActivatedRouteStub;

  const correctUser: User = {
    id: 44, username: 'carlo', password: 'serverPwd'
    , name: 'Carlo Cottura', specialUser: true, itemName: this.name
  };

  class MockDataService extends AbstractMockObservableService {

  }

  class MockAuthService extends AbstractMockObservableService {

    login(creds: LoginCredentials) {
      return Observable.of(JWT_TOKEN_HUGO);
    }

    checkAuth() {
      return this;
    }

  }

  class MockOAuthService {
    public hasValTokenRet: boolean;
    public identityClaims: any;

    hasValidAccessToken() {
      return this.hasValTokenRet;
    }

    fetchTokenUsingPasswordFlowAndLoadUserProfile() {
      return new Promise<string>((resolve, reject) => {
        resolve('fetched');
      });
    }

    getIdentityClaims() {
      return this.identityClaims;
    }
  }


  let mockAuthService;
  let mockOAuthService;
  let mockDataService;
  let sessionContext: SessionContext;

  beforeEach(async(() => {
    mockAuthService = new MockAuthService();
    mockOAuthService = new MockOAuthService();
    mockDataService = new MockDataService();
    sessionContext = new SessionContext();
    router = new FakeRouter() as any as Router;
    activatedStub = new ActivatedRouteStub();

    TestBed.configureTestingModule({
      declarations: [LoginComponent,
        MockComponent({ selector: 'app-version-info' })
      ],
      providers: [
        { provide: AuthenticationService, useValue: mockAuthService },
        { provide: Router, useValue: router },
        { provide: ActivatedRoute, useValue: activatedStub },
        { provide: SessionContext, useValue: sessionContext },
        { provide: BaseDataService, useValue: mockDataService }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    activatedStub.testParams = { fwdUrl: 'fwdUrlT', fwdId: 'fwdIdT' };
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;

    mockOAuthService.content = 'dummy';
    const claims = {
      given_name: 'Bruno',
      preferred_username: 'Haferkamp',
      role: ['testrole']
    };
    mockOAuthService.identityClaims = claims;
  });

  it('should set fwdUrl according to the url parameter', () => {
    spyOn(window, 'open');
    const fwdUrlVar = 'fwdUrlMock';
    const fwdIdVar = 'fwdIdMock';
    activatedStub.testQueryParamMap = { fwdUrl: fwdUrlVar, fwdId: fwdIdVar };
    fixture.detectChanges();
    sessionContext.setAccessToken(null);
    const user = correctUser;
    mockAuthService.content = user;

    component.login(user.name, user.password);
    fixture.detectChanges();

    const linkToOpen = fwdUrlVar + '?accessToken=' + sessionContext.getAccessToken() + '&fwdId=' + fwdIdVar;
    expect(window.open).toHaveBeenCalledWith(linkToOpen, '_blank');
  });

  it('should return on fwdUrl undefined', () => {
    spyOn(window, 'open');
    const fwdUrlVar = 'fwdUrlMock';
    const fwdIdVar = 'fwdIdMock';
    activatedStub.testQueryParamMap = { fwdId: fwdIdVar };
    fixture.detectChanges();
    sessionContext.setAccessToken(null);
    const user = correctUser;
    mockAuthService.content = user;

    component.login(user.name, user.password);
    fixture.detectChanges();

    expect(window.open).not.toHaveBeenCalled();
  });

  it('should throw an error while opening URL', () => {
    spyOn(window, 'open');
    const fwdUrlVar = 'fwdUrlMock';
    const fwdIdVar = 'fwdIdMock';
    activatedStub.testQueryParamMap = { fwdId: fwdIdVar };
    fixture.detectChanges();
    sessionContext.setAccessToken(null);
    mockAuthService.error = 'AuthService Error';
    const user = correctUser;

    component.login(user.name, user.password);
    fixture.detectChanges();

    expect(window.open).not.toHaveBeenCalled();
  });



  it('should have the access token in the SessionContext after login', () => {
    sessionContext.setAccessToken(null);
    const user = correctUser;

    component.login(user.name, user.password);
    fixture.detectChanges();

    expect(sessionContext.getAccessToken() !== null).toBe(true);

  });

  it('should show an error if the service return an error', () => {
    const user = correctUser;
    mockAuthService.error = 'AuthService Error';

    component.login(user.name, user.password);
    fixture.detectChanges();

    let des: DebugElement[];
    let elLocal: HTMLInputElement;
    des = fixture.debugElement.queryAll(By.css('.login-error-message'));
    expect(des.length).toBe(1);
    elLocal = des[0].nativeElement;
    expect(elLocal.hidden).toBeFalsy();
  });

  it('should should tell the ROUTER to navigate to Overview', () => {
    const user = correctUser;
    mockAuthService.content = user;
    spy = spyOn(router, 'navigate');

    component.login(user.name, user.password);
    fixture.detectChanges();

    const navArgs = spy.calls.mostRecent().args[0];
    expect(navArgs[0] === '/overview').toBe(true);
  });


});
