/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
export class Globals {
    static FRONTEND_VERSION = '1.1.1';
    static SESSION_TOKEN_TAG = 'X-XSRF-TOKEN';
    static CURRENT_USER = 'CURRENT_USER';
    static BASE_URL = '/elogbook/rest/beservice';
    static BASE_PORTAL_URL = '/portal/rest/beservice';
    static BRANCHESNAME = 'BRANCHES';
    static STATUSES = 'STATUSES';
    static GRID_TERRITORIES = 'TERRITORIES';
    static ACCESS_TOKEN = 'ACCESS_TOKEN';
    static FORWARD_URL = 'FORWARD_URL';
    static URL_PARAMS = 'URL_PARAMS';
    static BRANCHES = class Branches {
        static power = 'S';
        static gas = 'G';
        static heating = 'F';
        static water = 'W';

    };
    static LOCALSTORAGE_SESSION_ID = '/elogbook/session-id';
}
