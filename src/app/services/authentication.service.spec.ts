/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpModule, Http, XHRBackend, Response, ResponseOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

import { AuthenticationService } from './authentication.service';
import { LoginCredentials } from '../model/login-credentials';
import { User } from '../model/user';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';

describe('Http-AuthenticationService (mockBackend)', () => {
  let sessionContext: SessionContext;

  beforeEach( async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpModule ],
      providers: [
        AuthenticationService,
        { provide: XHRBackend, useClass: MockBackend },
        SessionContext
      ]
    })
    .compileComponents();
    sessionContext = new SessionContext();
  }));

  it('can instantiate service when inject service',
    inject([AuthenticationService], (service: AuthenticationService) => {
      expect(service instanceof AuthenticationService).toBe(true);
  }));



  it('can instantiate service with "new"', inject([Http], (http: Http) => {
    expect(http).not.toBeNull('http should be provided');
    const service = new AuthenticationService(http, sessionContext );
    expect(service instanceof AuthenticationService).toBe(true, 'new service should be ok');
  }));


  it('can provide the mockBackend as XHRBackend',
    inject([XHRBackend], (backend: MockBackend) => {
      expect(backend).not.toBeNull('backend should be provided');
  }));

  describe('when login()', () => {
    let backend: MockBackend;
    let service: AuthenticationService;
    let response: Response;
    const fakeCreds: LoginCredentials = { userName: 'carlo', password: 'newPwd'};
    const fakeUser: User = { id: 44,  username: 'carlo', password: 'serverPwd'
                         , name: 'Carlo Cottura', specialUser: true, itemName: this.name };


    beforeEach(inject([Http, XHRBackend], (http: Http, be: MockBackend) => {
      backend = be;
      service = new AuthenticationService(http, sessionContext);
      const successHeaders: Headers = new Headers();
      successHeaders.append(Globals.SESSION_TOKEN_TAG, 'SuperVALID!');
      const options = new ResponseOptions({status: 200, body: fakeUser, headers: successHeaders} );
      response = new Response(options);
    }));
    it('should have expected fake user (then)', async(inject([], () => {
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(response));

        service.login( fakeCreds ).toPromise()
          .then( user => {
            expect(user.id).toBe(44);
            expect(user.username).toBe('carlo');
            expect(user.password).toBe('serverPwd');
            expect(user.name).toBe('Carlo Cottura');
            expect(user.specialUser).toBe(true);
            expect(sessionContext.getCurrSessionId()).toBe('SuperVALID!');
          });
    })));

    it('should treat 404 as an Observable error', async(inject([], () => {
        const resp = new Response(new ResponseOptions({status: 404}));
        backend.connections.subscribe((c: MockConnection) => c.mockRespond(resp));

        service.login(fakeCreds)
          .do(user => {
            fail('should not respond with user');
          })
          .catch(err => {
            const str = err;
            expect(err).toMatch('Bad response status', 'should catch bad response status code');
            return Observable.of(null); // failure is the expected test result
          })
          .toPromise();
      })));
  });
 });
