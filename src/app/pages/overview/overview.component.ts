/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, AfterViewInit, ViewChildren, QueryList, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { SessionContext } from '../../common/session-context';
import { LogoutComponent } from '../../dialogs/logout/logout.component';
import { AlertComponent } from '../../dialogs/alert/alert.component';
import { MdDialog, MdDialogConfig } from '@angular/material';
import { User } from '../../model/user';
import { StatusEn, BannerMessageStatusEn } from '../../common/enums';
import { ModuleGridComponent } from '../module-grid/module-grid.component';
import { UserModuleService } from '../../services/user-module.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent {
  private dialogConfig = new MdDialogConfig();
  private bannerMessageStatus = BannerMessageStatusEn;

  constructor(
    private router: Router,
    public dialog: MdDialog,
    public sessionContext: SessionContext
  ) { }
  
}
