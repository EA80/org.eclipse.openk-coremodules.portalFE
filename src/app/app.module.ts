/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MdDialogModule } from '@angular/material';
import { MdGridListModule } from '@angular/material';
import { Daterangepicker } from 'ng2-daterangepicker';
import { HttpModule } from '@angular/http';
import { AuthenticationService } from './services/authentication.service';
import { BaseDataService } from './services/base-data.service';
import { SessionContext } from './common/session-context';
import { VersionInfoService } from './services/version-info.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OverviewComponent } from './pages/overview/overview.component';
import { LoginComponent } from './pages/login/login.component';
import { LogoutComponent } from './dialogs/logout/logout.component';
import { AlertComponent } from './dialogs/alert/alert.component';
import { VersionInfoComponent } from './common-components/version-info/version-info.component';
import { StringToDatePipe } from './common-components/pipes/string-to-date.pipe';
import { FormattedDatePipe } from './common-components/pipes/formatted-date.pipe';
import { FormattedTimestampPipe } from './common-components/pipes/formatted-timestamp.pipe';

import { Injectable } from '@angular/core';
import { BaseRequestOptions, Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { MainNavigationComponent } from './common-components/main-navigation/main-navigation.component';
import { ModuleGridComponent } from './pages/module-grid/module-grid.component';
import { UserModuleService } from './services/user-module.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OverviewComponent,
    LogoutComponent,
    AlertComponent,
    VersionInfoComponent,
    StringToDatePipe,
    FormattedDatePipe,
    FormattedTimestampPipe,
    MainNavigationComponent,
    ModuleGridComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    Daterangepicker,
    HttpModule,
    MdGridListModule,
    MdDialogModule.forRoot()
  ],
  entryComponents: [
    LogoutComponent,
    AlertComponent
  ],
  providers: [
    AuthenticationService,
    UserModuleService,
    VersionInfoService,
    SessionContext,
    BaseDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
