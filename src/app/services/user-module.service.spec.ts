/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpModule, Http, XHRBackend, Response, ResponseOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';

import { UserModuleService } from './user-module.service';
import { UserModule } from '../model/user-module';
import { SessionContext } from '../common/session-context';
import { Globals } from '../common/globals';

describe('Http-UserModuleService (mockBackend)', () => {

  let sessionContext: SessionContext;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        UserModuleService,
        { provide: XHRBackend, useClass: MockBackend },
        SessionContext
      ]
    })
      .compileComponents();
    sessionContext = new SessionContext();
  }));

  it('can instantiate service when inject service',
    inject([UserModuleService], (service: UserModuleService) => {
      expect(service instanceof UserModuleService).toBe(true);
    }));

  it('can instantiate service with "new"', inject([Http], (http: Http) => {
    expect(http).not.toBeNull('http should be provided');
    const service = new UserModuleService(http, sessionContext);
    expect(service instanceof UserModuleService).toBe(true, 'new service should be ok');
  }));


  it('can provide the mockBackend as XHRBackend',
    inject([XHRBackend], (backend: MockBackend) => {
      expect(backend).not.toBeNull('backend should be provided');
    }));

});