/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { NgModule, Component, OnInit, HostListener, Input } from '@angular/core';
import { MdGridListModule } from '@angular/material';
import { MdDialog, MdDialogConfig } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { SessionContext } from '../../common/session-context';
import { UserModule } from '../../model/user-module';
import { UserModuleService } from '../../services/user-module.service';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-module-grid',
  templateUrl: './module-grid.component.html',
  styleUrls: ['./module-grid.component.css']
})
export class ModuleGridComponent implements OnInit {

  @Input() moduleRights = true;
  userModules: UserModule[] = new Array();
  roles: string[] = new Array();

  constructor(public sessionContext: SessionContext,
    private userModuleService: UserModuleService,
    private authService: AuthenticationService) { }

  ngOnInit() {
    this.getUserModules();
  }

  goToElogbookIfValid(userModule: UserModule): void {
    const elogbookLink = userModule.link + '?accessToken=' + this.sessionContext.getAccessToken();

    this.authService.checkAuth().subscribe(res => {
      window.open(elogbookLink, '_blank');
    },
      error => {
        console.log(error);
      });
  }

  getUserModules(): void {
    this.userModuleService.getUserModulesForUser().subscribe(userModuleResult => {
      this.setModulesForRole(userModuleResult);
    },
      error => {
        console.log(error);
      });
  }

  setModulesForRole(userModuleResult): void {
    this.roles = this.sessionContext.getAccessTokenDecoded().roles;

    for (const umr of userModuleResult) {
      if (this.roles.indexOf(umr.requiredRole) !== -1) {
        this.userModules.push(umr);
        this.moduleRights = true;
      }
    }
    if (this.userModules.length === 0) {
      console.log('Der Benutzer hat keine Rechte');
      this.moduleRights = false;
    }
  }

  @HostListener('window:focus', ['$event'])
  onFocus(event: any): void {
    this.authService.checkAuth().subscribe(res => { },
      error => {
        console.log(error);
      });
  }

}

