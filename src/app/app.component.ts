/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionContext } from './common/session-context';
import { Globals } from './common/globals';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'app works!';
  

  constructor(private http: Http, public router: Router,
    public sessionContext: SessionContext) {

    this.http.get('assets/settings.json')
    .subscribe(res => this.sessionContext.settings = res.json());   

    this.sessionContext.centralHttpResultCode$.subscribe(rc => {
      this.onRcFromHttpService(rc);
    });

  }

  ngOnInit() {    
  }


  private onRcFromHttpService(rc: number): void {
    if (rc === 401) {
      this.router.navigate(['/login']);
    }

  }


}
