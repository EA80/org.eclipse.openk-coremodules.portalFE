/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { UUID } from 'angular2-uuid';
import { Globals } from '../common/globals';
import { SessionContext } from '../common/session-context';


@Injectable()
export class BaseHttpService {

  constructor(  ) {
  }

  protected getBaseUrl(): string {
      return Globals.BASE_PORTAL_URL;
  }

  protected  createCommonHeaders(headers: Headers, sessionContext: SessionContext) {
        headers.append('Accept', 'application/json');
        headers.append('content-Type', 'application/json');
        headers.set('Authorization', 'Bearer ' + sessionContext.getAccessToken());
        headers.append('unique-TAN', UUID.UUID());
        if ( sessionContext.getCurrSessionId() !== null ) {
            headers.append(Globals.SESSION_TOKEN_TAG, sessionContext.getCurrSessionId());
        }
  }

  protected extractData(res: Response, _sessContext: SessionContext ) {

    // let the interested 'people' know about our result
    _sessContext.centralHttpResultCode$.emit( res.status );

    if (res.status !== 302 && (res.status < 200 || res.status >= 300) ) {
        throw new Error('Bad response status: ' + res.status);
    }

    const data = res.json();
    return data || { };
}

  protected extractSessionId( headers: Headers, sessionContext: SessionContext ) {
        if ( headers != null ) {
            if ( headers.has( Globals.SESSION_TOKEN_TAG ) ) {
                sessionContext.setCurrSessionId(headers.get( Globals.SESSION_TOKEN_TAG ));
            }
        }
  }

  protected handleErrorPromise(error: any, _sessContext: SessionContext)  {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            let body: any = {};
            try {
                body = error.json() || '';
                const err = body.error || JSON.stringify(body);
                errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
            } catch ( exc ) {

            }
            
            // let the interested 'people' know about our result
            _sessContext.centralHttpResultCode$.emit( error.status );
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }  

}
