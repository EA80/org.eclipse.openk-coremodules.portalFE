/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { async, fakeAsync, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { By } from '@angular/platform-browser';
import { click } from '../../testing/index';
import { DebugElement, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { MdGridListModule } from '@angular/material';
import { MaterialModule, MdDialog, MdDialogConfig } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { MockComponent } from '../../testing/mock.component';
import { SessionContext } from '../../common/session-context';
import { UserModule } from '../../model/user-module';
import { UserModuleService } from '../../services/user-module.service';
import { ModuleGridComponent } from './module-grid.component';
import { USER_MODULES, ONE_USER_MODULE, PGM_MODULE } from '../../testing/user-module';
import { AuthenticationService } from '../../services/authentication.service';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

describe('ModuleGridComponent', () => {
  const sessionContext: SessionContext = new SessionContext();
  let component: ModuleGridComponent;
  let fixture: ComponentFixture<ModuleGridComponent>;
  let router: Router;
  let routerStub: FakeRouter;

  class MockAuthService extends AbstractMockObservableService {

    logout() {
      return this;
    }

    checkAuth() {
      return this;
    }

  }

  routerStub = {
    navigate: jasmine.createSpy('navigate').and.callThrough()
  };

  let mockAuthService;

  beforeEach(async(() => {
    mockAuthService = new MockAuthService();
    router = new FakeRouter() as any as Router;
    TestBed.configureTestingModule({
      imports: [
        MdGridListModule,
        BrowserAnimationsModule,
        MaterialModule.forRoot()
      ],
      declarations: [
        ModuleGridComponent,
        MockComponent({ selector: 'input', inputs: ['options'] })
      ],
      providers: [
        { provide: AuthenticationService, useValue: mockAuthService },
        { provide: SessionContext, useValue: sessionContext },
        { provide: UserModuleService, useClass: UserModuleService },
        { provide: 'Window', useValue: window }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  xit('should navigate to elogbookFE after click on its link', async(() => {
    component.userModules = USER_MODULES;
    /*
    spyOn($window, 'open').and.callFake( function() {
      return true;
    });
    */
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      let des: DebugElement[];

      des = fixture.debugElement.queryAll(By.css('md-grid-tile'));
      click(des[0]);
      // open new tab with /elogbookFE, how to test this?
      expect(window.location.href.endsWith('/elogbookFE')).toBeTruthy();
    });
  }));

  it('should check if the user has the rights for the roles', async(() => {

    component.roles = ['planned-policies-normaluser', 'planned-policies-access',
      'offline_access', 'uma_authorization', 'elogbook-access', 'elogbook-normaluser'];

    /* tslint:disable:max-line-length */
    sessionContext.setAccessToken('eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0eVByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiJhNTkzMWYzMS1lYjgzLTQ1Y2YtYWIwMy1kY2E2ODc5Y2M2YjgiLCJleHAiOjE1MjM5NTk5MTcsIm5iZiI6MCwiaWF0IjoxNTIzOTU5NjE3LCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vsb2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6ImMyZTlkN2FlLTJiZmEtNDU3OC1iMDllLWY1ZGM1ZjA5YTg3OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiIzZTM1ODg0OC0wNDY0LTQ2YjMtYmNlNC0wNDgyZDZkNWViY2QiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImVsb2dib29rLWFjY2VzcyIsImVsb2dib29rLW5vcm1hbHVzZXIiLCJwbGFubmVkLXBvbGljaWVzLWFjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5lZC1wb2xpY2llcy1ub3JtYWx1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbcGxhbm5lZC1wb2xpY2llcy1ub3JtYWx1c2VyLCBwbGFubmVkLXBvbGljaWVzLWFjY2Vzcywgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBlbG9nYm9vay1hY2Nlc3MsIGVsb2dib29rLW5vcm1hbHVzZXJdIiwibmFtZSI6Ik90dG8gTm9ybWFsdmVyYnJhdWNoZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJvdHRvIiwiZ2l2ZW5fbmFtZSI6Ik90dG8iLCJmYW1pbHlfbmFtZSI6Ik5vcm1hbHZlcmJyYXVjaGVyIn0.rKZdROPPD6xkSoNU9oY2vWYvTQnPKbHFh25o3PdOFEkEk1jmagEZCayKg2NH7TwVqJRZQfkyBy0PCfj9ReaQFfrKweTQZHUEi5bJuvzL4dR-TdDa_yGafCgnDvdM6vgshAdlvb5_8rPz5IEYfeszsC3RoLFBR13hRzu88MIFT2LJBaLLS4A386SznoN8T0wcC6q7uzfJRL-QAQoUkqjvSK1HM4yN4wzjj9IC-1MIye1uqcMfvd3qDHlzWlLM1vO9adcd5GZ8aaYg87a7m-vznJd1VtvaQ-l8TqYXa59z0Wgoyzr1UC3r_-1UlP9W6K0abiYTRDxvfIsvfdim4HozaA');
    component.userModules = USER_MODULES;

    component.setModulesForRole(USER_MODULES);

    expect(component.moduleRights).toBeTruthy();

  }));

  it('should check if the module list is empty', async(() => {

    component.roles = ['planned-policies-normaluser', 'planned-policies-access',
      'offline_access', 'uma_authorization', 'elogbook-access', 'elogbook-normaluser'];

    /* tslint:disable:max-line-length */
    sessionContext.setAccessToken('eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0eVByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiJhNTkzMWYzMS1lYjgzLTQ1Y2YtYWIwMy1kY2E2ODc5Y2M2YjgiLCJleHAiOjE1MjM5NTk5MTcsIm5iZiI6MCwiaWF0IjoxNTIzOTU5NjE3LCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vsb2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6ImMyZTlkN2FlLTJiZmEtNDU3OC1iMDllLWY1ZGM1ZjA5YTg3OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiIzZTM1ODg0OC0wNDY0LTQ2YjMtYmNlNC0wNDgyZDZkNWViY2QiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImVsb2dib29rLWFjY2VzcyIsImVsb2dib29rLW5vcm1hbHVzZXIiLCJwbGFubmVkLXBvbGljaWVzLWFjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5lZC1wb2xpY2llcy1ub3JtYWx1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbcGxhbm5lZC1wb2xpY2llcy1ub3JtYWx1c2VyLCBwbGFubmVkLXBvbGljaWVzLWFjY2Vzcywgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBlbG9nYm9vay1hY2Nlc3MsIGVsb2dib29rLW5vcm1hbHVzZXJdIiwibmFtZSI6Ik90dG8gTm9ybWFsdmVyYnJhdWNoZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJvdHRvIiwiZ2l2ZW5fbmFtZSI6Ik90dG8iLCJmYW1pbHlfbmFtZSI6Ik5vcm1hbHZlcmJyYXVjaGVyIn0.rKZdROPPD6xkSoNU9oY2vWYvTQnPKbHFh25o3PdOFEkEk1jmagEZCayKg2NH7TwVqJRZQfkyBy0PCfj9ReaQFfrKweTQZHUEi5bJuvzL4dR-TdDa_yGafCgnDvdM6vgshAdlvb5_8rPz5IEYfeszsC3RoLFBR13hRzu88MIFT2LJBaLLS4A386SznoN8T0wcC6q7uzfJRL-QAQoUkqjvSK1HM4yN4wzjj9IC-1MIye1uqcMfvd3qDHlzWlLM1vO9adcd5GZ8aaYg87a7m-vznJd1VtvaQ-l8TqYXa59z0Wgoyzr1UC3r_-1UlP9W6K0abiYTRDxvfIsvfdim4HozaA');
    component.userModules = [];

    component.setModulesForRole(USER_MODULES);
    expect(component.moduleRights).toBeFalsy();

  }));

  it('should create elogbookLink token string', async(() => {

    /* tslint:disable:max-line-length */
    sessionContext.setAccessToken('eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJodVl0eVByUEVLQ1phY3FfMW5sOGZscENETnFHdmZEZHctYUxGQXNoWHZVIn0.eyJqdGkiOiJhNTkzMWYzMS1lYjgzLTQ1Y2YtYWIwMy1kY2E2ODc5Y2M2YjgiLCJleHAiOjE1MjM5NTk5MTcsIm5iZiI6MCwiaWF0IjoxNTIzOTU5NjE3LCJpc3MiOiJodHRwOi8vZW50amF2YTAwMjo4MDgwL2F1dGgvcmVhbG1zL2Vsb2dib29rIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6ImMyZTlkN2FlLTJiZmEtNDU3OC1iMDllLWY1ZGM1ZjA5YTg3OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiIzZTM1ODg0OC0wNDY0LTQ2YjMtYmNlNC0wNDgyZDZkNWViY2QiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImVsb2dib29rLWFjY2VzcyIsImVsb2dib29rLW5vcm1hbHVzZXIiLCJwbGFubmVkLXBvbGljaWVzLWFjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5lZC1wb2xpY2llcy1ub3JtYWx1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbcGxhbm5lZC1wb2xpY2llcy1ub3JtYWx1c2VyLCBwbGFubmVkLXBvbGljaWVzLWFjY2Vzcywgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBlbG9nYm9vay1hY2Nlc3MsIGVsb2dib29rLW5vcm1hbHVzZXJdIiwibmFtZSI6Ik90dG8gTm9ybWFsdmVyYnJhdWNoZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJvdHRvIiwiZ2l2ZW5fbmFtZSI6Ik90dG8iLCJmYW1pbHlfbmFtZSI6Ik5vcm1hbHZlcmJyYXVjaGVyIn0.rKZdROPPD6xkSoNU9oY2vWYvTQnPKbHFh25o3PdOFEkEk1jmagEZCayKg2NH7TwVqJRZQfkyBy0PCfj9ReaQFfrKweTQZHUEi5bJuvzL4dR-TdDa_yGafCgnDvdM6vgshAdlvb5_8rPz5IEYfeszsC3RoLFBR13hRzu88MIFT2LJBaLLS4A386SznoN8T0wcC6q7uzfJRL-QAQoUkqjvSK1HM4yN4wzjj9IC-1MIye1uqcMfvd3qDHlzWlLM1vO9adcd5GZ8aaYg87a7m-vznJd1VtvaQ-l8TqYXa59z0Wgoyzr1UC3r_-1UlP9W6K0abiYTRDxvfIsvfdim4HozaA');

    component.goToElogbookIfValid(PGM_MODULE);
    expect(component.goToElogbookIfValid(PGM_MODULE)).toHaveBeenCalled;

  }));


});
